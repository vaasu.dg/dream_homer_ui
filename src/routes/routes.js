import HomeComponent from '../components/HomeComponent/HomeComponent';
import NotFound from '../components/common/notFound/notFound';
import DetailComponent from '../pages/detailPage/DetailPage';
// import PostingPropertyComponent from '../components/PostingPropertyComponent/PostingPropertyComponent'

const routes = [
    {
        path: '/',
        component: HomeComponent,
        title: 'Home'
    },
    {
        path: '/detail_page/:propertyId',
        component: DetailComponent,
        title: 'Home',
        exact: true
    },
    {
        path: '/posting_propertiy',
        title: 'Add Property',
        // component: PostingPropertyComponent
        isProducted: true
    },
    {
        path: '/user_dashboard',
        title: 'Dashboard',
        isProducted: true
    },
    {
        path: '/user_history',
        title: 'History',
        isProducted: true
    },
    {
        path: "**",
        component: () => <NotFound />
    }
];

export default routes;