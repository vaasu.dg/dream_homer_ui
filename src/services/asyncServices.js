import axios, { get, post } from 'axios';

axios.defaults.baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:5000/' : 'https://bangalore-properties-mw.herokuapp.com/'

console.log(process.env.NODE_ENV)
export const userProfile = async (_, uri, dispatch = () => { }, type = {}) => {
    try {
        if (!sessionStorage.length) return;
        const { data: user } = await get(uri);
        return user;
    } catch (error) {
        dispatch({ type, errorMessage: error, loading: false });
    }
}

export const getAllProperties = async (_, uri, dispatch = () => { }, type = {}) => {
    try {
        const { data } = await get(uri);
        return data;
    } catch (error) {
        dispatch({ type, errorMessage: error, loading: false });
    }
}


export const loginUser = async (uri, dispatch, type, response) => {
    try {
        const { data: { googleId } } = await post(uri, response);
        dispatch({ type, tokenId: googleId, loading: false, isLoggedIn: true });
    } catch (error) {
        console.error(error);
        dispatch({ type, errorMessage: error, loading: false });
    }
}

export const addProperty = async ({ _, uri, dispatch = () => { }, type = {}, response }) => {
    dispatch({ type, loading: true, isLoggedIn: false });
    try {
        const data = await axios.post(uri, response);
        dispatch({ type, loading: false });
        return data;
    } catch (error) {
        dispatch({ type, errorMessage: error, loading: false });
    }
}

export const addFav = async ({ _, uri, dispatch = () => { }, type = {}, response }) => {
    dispatch({ type, loading: true, isLoggedIn: false });
    try {
        const data = await axios.patch(uri, response);
        dispatch({ type, loading: false });
        return data;
    } catch (error) {
        dispatch({ type, errorMessage: error, loading: false });
    }
}