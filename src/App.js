import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import IndexPage from './pages';
import Store from './stores/Store';


function App() {

  return (
    <>
      <Store>
        <BrowserRouter>
          <IndexPage />
        </BrowserRouter>
      </Store>
    </>
  );
}

export default App;
