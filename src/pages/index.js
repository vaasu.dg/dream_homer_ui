import React, { useLayoutEffect } from 'react';
import { Route, Switch, useLocation } from "react-router-dom";
import PrivatePages from '../pages/privatePages/PrivatePages';
import routes from '../routes/routes';
import Home from './home/home';

const IndexPage = () => {
    const { pathname } = useLocation()

    useLayoutEffect(() => {
        window.scrollTo(0, 0);
    }, [pathname])

    return (
        <Home>
            <Switch>
                {routes.map((route, indx) => (
                    <Route
                        exact
                        path={route.path}
                        component={!route.isProducted ? route.component : () => <PrivatePages />}
                        key={indx}
                    />
                ))}
            </Switch>
        </Home>
    )
}

export default IndexPage;