import React from 'react';
import { useQuery } from 'react-query';
import { getAllProperties } from '../../services/asyncServices';
import GalleryComponent from '../../components/common/gallery/Gallery';
import { getUserID } from '../../utils/storage.utils';

const DashboardComponent = () => {
    const { data, isLoading } = useQuery(['getAllProperties', `/api/property/list?googleId=${getUserID('token-id')}`], getAllProperties);
    return <GalleryComponent data={data} isLoading={isLoading} isMoreBtn={false} />
};

export default DashboardComponent;