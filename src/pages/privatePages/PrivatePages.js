import React from 'react';
import { Route, Switch } from "react-router-dom";
import ConditionalView from '../../components/common/ConditionalView';
import UnAuth from '../../components/common/unAuth/UnAuth';
import PostingPropertyComponent from '../../components/PostingPropertyComponent/PostingPropertyComponent';
import DashboardComponent from '../../pages/dashboard/dashboard';
import { getUserID } from '../../utils/storage.utils';
import UserHistory from '../userHistory/UserHistory';


const PrivatePages = () => {
    if (Boolean(!getUserID('token-id'))) return <UnAuth />

    return (
        <ConditionalView visible={Boolean(getUserID('token-id'))}>
            <Switch>
                <Route path='/user_history' component={UserHistory} />
                <Route path='/posting_propertiy' component={PostingPropertyComponent} />
                <Route path='/user_dashboard' component={DashboardComponent} />
            </Switch>
        </ConditionalView>
    )
}

export default PrivatePages;