import React, { useContext } from 'react';
import FooterComponent from '../../components/Footer/Footer';
import HeaderComponent from '../../components/Header/Header';
import './home.css';



const HomePage = ({ children }) => {

    return (
        <div className='bs__home'>
            <HeaderComponent />
            <main>
                {children}
            </main>
            <FooterComponent />
        </div>
    )
};


export default HomePage;

