import React from 'react';
import { useQuery } from 'react-query';
import { getAllProperties, userProfile } from '../../services/asyncServices';
import { getUserID } from '../../utils/storage.utils';
import { Link, useHistory } from 'react-router-dom';
import LoaderComponent from '../../components/common/Loader';
import ConditionalView from '../../components/common/ConditionalView';
import './DetailPage.css'
import Axios from 'axios';

const DetailComponent = () => {
    const { location: { state: { propertyId } = {} } } = useHistory();
    console.log(useHistory())
    const { data, isLoading } = useQuery(['getSingleProperty', `/api/property/list?propertyId=${propertyId}`], getAllProperties);

    const property = data?.properties[0];
    return (
        <div className='detail__page'>
            <ConditionalView visible={isLoading}>
                <LoaderComponent />
            </ConditionalView>
            <p className='detail__page_title'>{property?.propertyTitle}</p>
            <p className='detail__page_price'>Price: <span>{property?.propertyPrice}</span></p>
            <img src={property?.images?.large} alt={property?.propertyTitle} />
            <div className='detail__page_footer'>
                <div>
                    <p>BHK: <span>{property?.propertyConfiguration}</span></p>
                    <p>Price: <span>{property?.propertyPrice}</span></p>
                </div>
                <div>
                    <p>Type: <span>{property?.propertyType}</span></p>
                    <p>Square Feet: <span>{property?.sqFt}</span></p>
                </div>
                <div>
                    <p>Address: <span>{property?.contact?.address.propertyArea}</span></p>
                    <p>Pincode: <span>{property?.contact?.address.propertyPincode}</span></p>
                </div>
            </div>
            <div>
                <Link to='/'>Back</Link>
            </div>
        </div>

    )
};

export default DetailComponent;