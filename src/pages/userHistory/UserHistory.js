import React from 'react';
import { useQuery } from 'react-query';
import { getAllProperties } from '../../services/asyncServices';
import GalleryComponent from '../../components/common/gallery/Gallery';
import { getUserID } from '../../utils/storage.utils';
import { userProfile } from '../../services/asyncServices';

const UserHistory = () => {
    const {
        data: { user: { _id } = {} } = {}
    } = useQuery(['userProfile', `/user/${getUserID('token-id')}`], userProfile);

    const { data, isLoading } = useQuery(['getAllProperties', `/api/property/list?userId=${_id}`], getAllProperties);

    return <GalleryComponent data={data} isLoading={isLoading} isMoreBtn={false}/>
}

export default UserHistory;