import React, { createContext, useReducer } from 'react';
import userReducer from './reducers/userReducer';
export const UserContext = createContext();

const Store = ({ children }) => {

    const initialState = {
        loading: true,
        isLoggedIn: false,
        errorMessage: '',
        tokenId: '',
        userData: [],
        propertyData: []
    }

    const [state, dispatch] = useReducer(userReducer, initialState);

    return (
        <UserContext.Provider
            value={{ state, dispatch }}
        >
            {children}
        </UserContext.Provider>
    )
}

export default Store;
