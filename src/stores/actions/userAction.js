export const LOADING = 'LOADING';
export const LOGIN_USER = 'LOGIN_USER';
export const USER_PROFILE = 'USER_PROFILE';
export const GET_ALL_PROPERTIES = 'GET_ALL_PROPERTIES';
export const ADD_PROPERTY = 'ADD_PROPERTY';