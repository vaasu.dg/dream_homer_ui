import {
    LOADING,
    LOGIN_USER,
    USER_PROFILE,
    GET_ALL_PROPERTIES,
    ADD_PROPERTY
} from '../actions/userAction';

export default function userReducer(state, action) {
    switch (action.type) {
        case LOADING:
            return { loading: action.loading, isLoggedIn: action.isLoggedIn }
        case LOGIN_USER:
            return { loading: action.loading, tokenId: action.tokenId, isLoggedIn: action.isLoggedIn }
        case USER_PROFILE:
            return { loading: action.loading, userData: action.userData }
        case GET_ALL_PROPERTIES:
            return { loading: action.loading, propertyData: action.propertyData }
        case ADD_PROPERTY:
            return { loading: action.loading, errorMessage: action.errorMessage }
        default:
            return state
    }
}