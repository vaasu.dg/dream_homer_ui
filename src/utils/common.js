import { getUserID } from './storage.utils';

export function hideComponentForAuth(isSameGoogleId) {
    return isSameGoogleId !== getUserID('token-id');
}

export function urlMask(url) {
    return url.replace(/ /g, '-').toLowerCase();
}