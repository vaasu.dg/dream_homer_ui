export function saveUserID(id, values) {
    try {
        window.sessionStorage.setItem(id, values);
    } catch (error) {
        console.error(error);
    }
}

export function getUserID(id) {
    try {
        const token = window.sessionStorage.getItem(id);
        if (!token) return null;
        return token;
    } catch (error) {
        console.error(error)
    }
}

export function clearUserID(id) {
    try {
        window.sessionStorage.removeItem(id);
    } catch (error) {
        console.error(error);
    }
}

export function saveIdLocalStorate(id, values) {
    try {
        window.localStorage.setItem(id, values);
    } catch (error) {
        console.error(error);
    }
}