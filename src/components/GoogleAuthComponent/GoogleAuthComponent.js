import React, { useContext } from 'react';
import GoogleLogin from 'react-google-login';
import { loginUser } from '../../services/asyncServices';
import { LOGIN_USER } from '../../stores/actions/userAction';
import { UserContext } from '../../stores/Store';
import { clearUserID, getUserID, saveUserID } from '../../utils/storage.utils';
import './GoogleAuthComponent.css';
import gLogo from '../../assets/images/gLogo.png';
import { useHistory } from 'react-router-dom';


const LoginButton = ({ onClick, disabled }) => <span style={{ backgroundImage: `url(${gLogo})` }} className={disabled ? 'google__btn__disabled' : ''} onClick={onClick} disabled={disabled}>Login with Google</span>

const GoogleAuthComponent = ({ session }) => {
    const context = useContext(UserContext);
    const { dispatch } = context;
    const { push } = useHistory();

    const responseGoogle = async ({ profileObj }) => {
        const { name, email, imageUrl, googleId } = await profileObj || {};
        try {
            await loginUser('/user', dispatch, LOGIN_USER, { name, email, googleId, imageUrl, googleId });
            saveUserID('token-id', googleId);
            push('/');
        } catch (error) {
            console.error(error);
            clearUserID('token-id');
        }
        session(Boolean(getUserID('token-id')));
    }

    return (

        <GoogleLogin
            clientId='995643456611-b5hll9n9jfb67j79nev53r9rap3q6ucs.apps.googleusercontent.com'
            onSuccess={responseGoogle}
            onFailure={responseGoogle}
            autoLoad={false}
            cookiePolicy={'single_host_origin'}
            render={({ onClick, disabled }) => (<LoginButton onClick={onClick} disabled={disabled} />)}
        />
    );
}

export default GoogleAuthComponent;
