import React from 'react';
import Loader from 'react-loader-spinner';

const LoaderComponent = () => {
    return (
        <div className='loader__wrapper'>
            <div className='loader'>
                <Loader
                    type='ThreeDots'
                    color='#FFCC01'
                    height={100}
                    width={100}
                />
            </div>
        </div>
    )
}

export default LoaderComponent;