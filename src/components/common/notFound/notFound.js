import React from 'react';

const NotFound = () => {
    return (
        <div className='un__auth'>
            <p>Sorry, This page not available currently</p>
            <p>Please contact our team</p>
        </div>
    )
}

export default NotFound;