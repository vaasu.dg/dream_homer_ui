import React, { useState } from 'react'
import ConditionalView from './ConditionalView';

const MessageBoardComponent = ({ title, bgColor, color, timer = 4000 }) => {
    const [hide, setHide] = useState(false);
    setTimeout(() => {
        setHide(true)
    }, timer);

    return (
        <ConditionalView visible={!hide}>
            <div className='message__board'>
                <p className='message__board_title' style={{ backgroundColor: bgColor, color }}>{title}</p>
            </div>
        </ConditionalView>
    )
}
export default MessageBoardComponent;