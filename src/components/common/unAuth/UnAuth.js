import React from 'react';
import './unAuth.css';

const UnAuth = () => {
    return (
        <div className='un__auth'>
            <p>Please login to access this page.</p>
        </div>
    )
}

export default UnAuth;