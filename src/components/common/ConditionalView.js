import React from 'react';

const ConditionalView = ({ visible, children }) => visible ? <>{children}</> : null;

export default ConditionalView;