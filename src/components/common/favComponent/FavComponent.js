import React, { useState } from 'react'
import bookmark__active from '../../../assets/images/bookmark__active.png';
import bookmark_normal from '../../../assets/images/bookmark_normal.png';
import { useLocation } from 'react-router-dom';
import { getUserID } from '../../../utils/storage.utils'
import ConditionalView from '../ConditionalView';
import './FavComponent.css';
import { hideFav } from '../../../utils/common'
import { addFav } from '../../../services/asyncServices';
import { useMutation } from 'react-query';
import { useHistory } from 'react-router-dom';
import MessageBoardComponent from '../messageBoard';


const FavComponent = ({ isVisible, propertyId, userId, isFav }) => {
    const { push } = useHistory();
    const [isLogged, setIsLogged] = useState(false)
    const addFavQuery = async (response) => addFav({ uri: 'user', response: response });

    const [mutate, { isLoading }] = useMutation(addFavQuery, {
        onSuccess: () => push('/user_history')
    })

    const addOnFav = async () => {
        const id = userId
        if (!id) {
            setIsLogged(true);
            setTimeout(() => { setIsLogged(false) }, 1000);
            return;
        }
        await mutate({ id, propertyId });
    }
    return (

        <ConditionalView visible={isVisible} propertyId={propertyId} userId={userId} isFav={isFav}>
            <ConditionalView visible={isLogged}>
                <MessageBoardComponent title='Please Login' bgColor='#f52803' timer={1000} />
            </ConditionalView>
            <ConditionalView visible={isLoading}>
                <MessageBoardComponent title='Loading' />
            </ConditionalView>
            <div
                onClick={addOnFav}
                className={isFav ? 'add__fav_wrapper active' : 'add__fav_wrapper'}
                style={{ backgroundImage: isFav ? `url(${bookmark__active})` : `url(${bookmark_normal})` }}
            >
            </div>
        </ConditionalView>
    )
}

export default FavComponent;