import React from 'react'

const RadioGroup = ({ data, name, title, value, onChange }) => {
    return (
        <>
        <h4 className='form__title'>{title}</h4>
            { data.length && data.map((radioBtn, i) => {
                const formatedLabel = radioBtn.split(' ').join('').toLowerCase();
                return (
                    <div key={i} className='input__group flex__input'>
                        <input type='radio' id={formatedLabel}
                            name={name} value={radioBtn} onChange={onChange} />
                        <label htmlFor={formatedLabel}>{radioBtn}</label>
                    </div>
                )
            })
            }
        </>
    )
}

export default RadioGroup