import React from 'react'

const Input = ({ name, placeholder, value, onChange, disabled }) => {
    return (
        <div className='input__group'>
            <input type='text' name={name} placeholder={placeholder} value={value} onChange={onChange} disabled={disabled} />
        </div>
    )
}

export default Input