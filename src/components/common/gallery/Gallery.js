import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { hideComponentForAuth, urlMask } from '../../../utils/common';
import ConditionalView from '../ConditionalView';
import FavComponent from '../favComponent/FavComponent';
import LoaderComponent from '../Loader';
import './Gallery.css';

const GalleryComponent = ({ data, isLoading, fav, userId, onPaginate, isMoreBtn }) => {
    const { properties, currentCount, totalCount } = data || {};

    const { push, location } = useHistory()
    const onCardClick = (propertyTitle, propertyId) => {
        push({
            pathname: `detail_page/${urlMask(propertyTitle)}`,
            state: {
                propertyId: propertyId
            }
        });
    }

    return (
        <div className='gallery'>
            <ConditionalView visible={isLoading}>
                <LoaderComponent />
            </ConditionalView>

            <ConditionalView visible={isMoreBtn}>
                <p className='gallery__title'>Total <span>{currentCount}/{totalCount}</span> Properties</p>
            </ConditionalView>
            <ConditionalView visible={!isMoreBtn}>
                <p className='gallery__title'>Total <span>{currentCount}</span> Properties</p>
            </ConditionalView>
            <div className='gallery__component'>
                {
                    properties?.map((property, indx) => {
                        return (
                            <div className='card__component' key={indx} >
                                <FavComponent
                                    isVisible={hideComponentForAuth(property?.googleId)}
                                    propertyId={property._id}
                                    userId={userId}
                                    isFav={fav?.includes(property._id) || location.pathname === '/user_history'}
                                />
                                <div className='clikable__card' onClick={() => onCardClick(property?.propertyTitle, property?._id)}>
                                    <div className='card__body'>
                                        <div className='card__header'>
                                            <div className='image'
                                                style={{ backgroundImage: `url(${property?.images?.thumbnail})` }}
                                            />
                                        </div>
                                        <div className='card__footer'>
                                            <div className='card__footer_detail'>
                                                <p className='card__footer_title'>{property?.propertyTitle}</p>
                                                <p className='card__footer_address'>
                                                    {property?.contact?.address?.propertyArea},
                                                {property?.contact?.address?.propertyPincode},
                                            </p>
                                                <div className='card__footer_type'>
                                                    <p className='property_config'>{property?.propertyConfiguration} //</p>
                                                    <p className='property_config'>Square Feet: {property?.sqFt} //</p>
                                                    <p className='property_config'>House Type: {property?.propertyType}</p>
                                                </div>
                                                <div className='card__footer_price'>
                                                    <p>Price: <span>{property?.propertyPrice}</span></p>
                                                </div>
                                                <ConditionalView visible={hideComponentForAuth(property?.googleId)}>
                                                    <div className='card__footer_contact'>
                                                        <Link
                                                            to={{
                                                                pathname: `detail_page/${urlMask(property?.propertyTitle)}`,
                                                                state: {
                                                                    propertyId: property._id
                                                                }
                                                            }}>
                                                            Contact
                                                    </Link>
                                                    </div>
                                                </ConditionalView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div >
            <ConditionalView visible={currentCount && isMoreBtn}>
                <div className='gallery__more'>
                    <button className='gallery__more_btn' onClick={onPaginate} disabled={currentCount >= totalCount}>More...</button>
                </div >
            </ConditionalView>
        </div >
    )
};

export default GalleryComponent;