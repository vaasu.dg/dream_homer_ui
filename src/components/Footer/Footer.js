import React from 'react';
import './Footer.css';

const FooterComponent = () => {
    return (
        <footer className='footer_component'>
            <div>
                <h4>About</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce laoreet vestibulum lectus, sed suscipit metus accumsan sit amet. Duis tempus accumsan metus sit amet laoreet. Maecenas vehicula semper lectus, vitae dignissim turpis tristique pellentesque. Suspendisse ut augue hendrerit, suscipit nibh id, mollis magna. Donec feugiat ornare magna in rutrum. Curabitur sed nunc malesuada, vehicula ex eu, rutrum ex. Sed gravida pharetra imperdiet. Morbi cursus magna nisl, eget pulvinar nibh tincidunt nec. Nullam interdum non neque vel sagittis. Ut quam purus, feugiat ut nunc eget, imperdiet varius dui. Aliquam pretium eros a dignissim ullamcorper. Sed eu risus nisl. Sed id facilisis augue, sit amet lobortis lacus. Morbi elementum, ligula ac vehicula ultricies, felis elit viverra lacus, et cursus lorem mauris et augue. Cras eleifend nisl turpis, ut convallis augue pellentesque nec.
                </p>
            </div>
            <div>
                <h4>Contact</h4>
                <div>About Us</div>
                <div>Location</div>
                <div>Contact Us</div>
            </div>
        </footer>
    )
}
export default FooterComponent;