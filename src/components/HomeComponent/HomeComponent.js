import React, { useState } from 'react';
import { usePaginatedQuery, useQuery } from 'react-query';
import { getAllProperties, userProfile } from '../../services/asyncServices';
import { getUserID } from '../../utils/storage.utils';
import GalleryComponent from '../common/gallery/Gallery';
import './HomeComponent.css';

const HomeComponent = () => {
    const {
        data: { user: { activities: { favourites } = {}, _id } = {} } = {}
    } = useQuery(['userProfile', `/user/${getUserID('token-id')}`], userProfile);

    const [limit, setLimit] = useState(10);

    const {
        resolvedData,
        latestData
    } = usePaginatedQuery(['', `/api/property/list?limit=${limit}`], getAllProperties);
    const onPaginate = () => setLimit(limit + 5);

    return <GalleryComponent
        data={resolvedData}
        isLoading={Boolean(!latestData)}
        fav={favourites}
        userId={_id}
        isMoreBtn={true}
        onPaginate={onPaginate}
    />
};

export default React.memo(HomeComponent);