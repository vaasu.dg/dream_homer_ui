import React, { useContext, useState } from 'react';
import { useMutation } from 'react-query';
import { useHistory } from 'react-router-dom';
import { addProperty } from '../../services/asyncServices';
import { UserContext } from '../../stores/Store';
import { getUserID } from '../../utils/storage.utils';
import ConditionalView from '../common/ConditionalView';
import Input from '../common/input';
import LoaderComponent from '../common/Loader';
import MessageBoardComponent from '../common/messageBoard';
import RadioGroup from '../common/radioGroup';

const PostingPropertyComponent = () => {
    const context = useContext(UserContext);
    const { dispatch } = context;
    const { push } = useHistory();

    const [thumbnail, setThumbnail] = React.useState();
    const uploadImage = e => setThumbnail(e.target.files[0]);

    /** sadly ...spread operator not working in my computer :(. So than below code... need to refactor */
    const [propertyTitle, setPropertyTitle] = useState('');
    const [propertyPrice, setPropertyPrice] = useState('');
    const [propertySqFt, setPropertySqFt] = useState('');
    const [propertyType, setPropertyType] = useState('');
    const [propertyConfiguration, setPropertyConfiguration] = useState('');
    const [propertyContact, setPropertyContact] = useState('');
    const [propertyArea, setPropertyArea] = useState('');
    const [propertyPincode, setPropertypincode] = useState('');

    const [asyncError, setAsyncError] = useState('');
    const [isLoaderVisible, setIsLoaderVisible] = useState('');


    const addPropertyQuery = async (response) => addProperty({ uri: 'api/property/add', response: response });

    const [mutate, { isLoading, isSuccess, isError, data: queryData, error, status }] = useMutation(addPropertyQuery, {
        onSuccess: e => {
            setIsLoaderVisible(false)
        }
    })

    const [errors, setErrors] = useState();
    const [errorMessage, setErrorMessage] = useState();

    const onAddProperty = async e => {
        e.preventDefault()
        setIsLoaderVisible(isLoading);
        const data = new FormData();
        // debugger
        data.append('propertyTitle', propertyTitle);
        data.append('propertyPrice', propertyPrice);
        data.append('sqFt', propertySqFt);
        data.append('propertyType', propertyType);
        data.append('propertyConfiguration', propertyConfiguration);
        data.append('propertyContact', propertyContact);
        data.append('propertyArea', propertyArea);
        data.append('propertyPincode', propertyPincode);
        data.append('thumbnail', thumbnail);
        data.append('googleId', getUserID('token-id'));


        /**
         * 
         * Sorry for very classic way form validation
         */
        const newError = [];


        if (data.get('thumbnail') === 'undefined') {
            newError.push('thumbnail')
        }

        for (var val of data.entries()) {
            if (val[1] === '' || val[1] === undefined || val[1] === null) {
                newError.push(val[0])
            }
            setErrors(newError)
            setErrorMessage(newError.join('').replace(/property/g, ' - ').toUpperCase());
        }

        setTimeout(() => { setErrors([]) }, 3000);

        if (!newError.length) {
            await mutate(data);
            dispatch({ type: 'ADD_PROPERTY', errorMessage: queryData, loading: false });
            push('/');
        }
    }

    return (
        <div className='layout__content'>
            <ConditionalView visible={errors?.length}>
                <MessageBoardComponent title={`Missing fields... ${errorMessage}`} bgColor='#F00' timer={5000} />
            </ConditionalView>
            <form method='POST' onSubmit={onAddProperty} autoComplete='off'>
                <h1 className='form__title'>Add Your Property Details<span>* All fields required</span></h1>
                <span style={{ color: 'tomato' }}>{JSON.stringify(errorMessage)}</span>
                <div className='input__group'>
                    <input type='file' name='thumbnail' onChange={uploadImage} />
                </div>
                <Input
                    name='propertyTitle'
                    placeholder='* ex: 3 BHK Near Marathalli'
                    onChange={e => setPropertyTitle(e.target.value)}
                />
                <Input
                    name='propertyPrice'
                    placeholder='* Price of the Property'
                    onChange={e => setPropertyPrice(e.target.value)}
                />
                <Input
                    name='sqFt'
                    placeholder='* Total Square Feet the Property'
                    onChange={e => setPropertySqFt(e.target.value)}
                />
                <RadioGroup
                    data={[
                        'Residential Apartment',
                        'Independent/Builder Floor',
                        'Independent House/Villa',
                        'Others']}
                    name='propertyType'
                    onChange={e => setPropertyType(e.target.value)}
                    title='* Choose the Type of Property'
                />
                <RadioGroup
                    data={[
                        '1 bhk',
                        '2 bhk',
                        '3 bhk',
                        '4 bhk',
                        '4+ bhk']}
                    name='propertyConfiguration'
                    onChange={e => setPropertyConfiguration(e.target.value)}
                    title='* No.of BHK'
                />
                <Input
                    name='propertyContact'
                    placeholder='* Contact Number'
                    onChange={e => setPropertyContact(e.target.value)}
                />
                <Input
                    name='propertyArea'
                    placeholder='* Located Area of the Property'
                    onChange={e => setPropertyArea(e.target.value)}
                />
                <Input
                    name='propertyPincode'
                    placeholder='* Pincode of the Location'
                    onChange={e => setPropertypincode(e.target.value)}
                />
                <div className='input__group'>
                    <button type='submit' title='' disabled={isLoading}>Submit</button>
                </div>
            </form>
            <ConditionalView visible={isLoaderVisible}>
                <LoaderComponent />
            </ConditionalView>
        </div>
    )
}

export default PostingPropertyComponent