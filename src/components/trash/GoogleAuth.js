import axios from 'axios';
import React from 'react';
import GoogleLogin from 'react-google-login';


function GoogleAuth() {
    const [img, setImg] = React.useState('')
    function responseGoogle(resp) {
        const { name, email, imageUrl, googleId } = resp.profileObj;
        axios.post('/user', { name, email, googleId })
        setImg(imageUrl)
    }
    return (
        <>
            <GoogleLogin
                clientId='995643456611-b5hll9n9jfb67j79nev53r9rap3q6ucs.apps.googleusercontent.com'
                buttonText='Vasu'
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy={'single_host_origin'}
                render={renderProps => (
                    <button onClick={renderProps.onClick} disabled={renderProps.disabled}>This is my custom Google button</button>
                )}
            />
            <img src={img} />
        </>
    );
}

export default GoogleAuth;
