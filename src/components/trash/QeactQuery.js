import axios from 'axios';
import React from 'react';
import { useQuery } from 'react-query';
import { ReactQueryDevtools } from 'react-query-devtools';


function Nav({ setPage }) {
    return (
        <div>
            <button onClick={() => setPage('planets')}>Planets</button>
            <button onClick={() => setPage('people')}>People</button>
        </div>
    )
}

const fetchPlantes = async () => {
    const res = await axios.get('./api');
    // console.log(res.data)
    return res.data;
}

const fetchPeople = async () => {
    const res = await axios.get('./api');
    return res.data;
}

const Planets = () => {
    const { status, data } = useQuery('planets', fetchPlantes, {
        staleTime: 1,
        onError: () => { console.log('Error notification') },
        onSuccess: () => { console.log('Success notification') }
    })
    console.log(data)
    return (
        <>
            Planets
            {status === 'loading' && <p>Loading</p>}
            {status === 'error' && <p>Error</p>}
            {status === 'success' &&
                <>
                    {data.results.map((result, ind) => (
                        <React.Fragment key={ind}>
                            <img width='100' src={result.thumbnail} alt={result.title} />
                        </React.Fragment>
                    ))}
                </>
            }
        </>
    )
}

const People = () => {
    const { status, data } = useQuery('planets', fetchPeople)
    console.log(data)
    return (
        <>
            Planets
            {status === 'loading' && <p>Loading</p>}
            {status === 'error' && <p>Error</p>}
            {status === 'success' &&
                <>
                    {data.results.map((result, ind) => (
                        <React.Fragment key={ind}>
                            <p>{result.title}</p>
                        </React.Fragment>
                    ))}
                </>
            }
        </>
    )
}

function ReactQueryComponent() {

    const [page, setPage] = React.useState('planets')

    return (
        <>
            <Nav setPage={setPage} />
            <h2>Starwar info</h2>
            {
                page === 'planets' ? <Planets /> : <People />
            }
            <ReactQueryDevtools />
        </>
    );
}

export default ReactQueryComponent;
