import React, { useState } from 'react';
import { useGoogleLogout } from 'react-google-login';
import { useQuery } from 'react-query';
import { Link, useHistory } from 'react-router-dom';
import logo from '../../assets/images/logo.png';
import { userProfile } from '../../services/asyncServices';
import { clearUserID, getUserID } from '../../utils/storage.utils';
import ConditionalView from '../common/ConditionalView';
import MessageBoardComponent from '../common/messageBoard';
import GoogleAuthComponent from '../GoogleAuthComponent/GoogleAuthComponent';
import './Header.css';



const HeaderComponent = () => {
    const [isLoggedIn, setIsLoggedIn] = useState(Boolean(getUserID('token-id')));
    const { push } = useHistory();
    const [isSearching, setIsSearching] = useState(false);

    const {
        data: { user } = {},
        isError,
        isSuccess
    } = useQuery(['userProfile', `/user/${getUserID('token-id')}`], userProfile);


    const onLogoutSuccess = _ => {
        clearUserID('token-id');
        setIsLoggedIn(false);
        push('/');
    }

    const { signOut } = useGoogleLogout({
        clientId: '995643456611-b5hll9n9jfb67j79nev53r9rap3q6ucs.apps.googleusercontent.com',
        onLogoutSuccess,
        onFailure: (error) => console.log('Error ', error)
    });
    const url = '4 Bedroom Independent House in 3rd Block Koramangala'
    return (
        <header className='header'>
            <div className='logo__col'>
                <img src={logo} style={{ marginRight: '2.5em' }} />
                <Link to='/' >Buy</Link>
                <Link to='/posting_propertiy'>Sell</Link>
            </div>
            <div className='search' >
                <input placeholder='Search' onChange={_ => { setIsSearching(true); setTimeout(() => { setIsSearching(false) }, 1000); }} />
            </div>
            <div className='user__nav'>
                <ConditionalView visible={isSearching}>
                    <MessageBoardComponent title='This feature will available in the future sprint' />
                </ConditionalView>
                <ul>
                    <ConditionalView visible={!isLoggedIn}>
                        <li>
                            <Link className='google_btn'>
                                <GoogleAuthComponent session={() => setIsLoggedIn(true)} />
                            </Link>
                        </li>
                    </ConditionalView>

                    <ConditionalView visible={isLoggedIn}>
                        <div>
                            <li className='google__profile'>
                                <Link >
                                    <span>{user?.name}</span>
                                </Link>
                                <img className='profile__image' src={user?.imageUrl} />
                                <ul>
                                    <li>
                                        <Link to='/user_dashboard'>My Ads</Link>
                                    </li>
                                    <li>
                                        <Link to='/user_history'>My Favorites</Link>
                                    </li>
                                    <li>
                                        <Link to='/user_setting'>Settings</Link>
                                    </li>
                                    <li>
                                        <Link onClick={signOut}>Logout</Link>
                                    </li>
                                </ul>
                            </li>

                        </div>

                    </ConditionalView>
                </ul>
            </div>

        </header >
    )
}

export default HeaderComponent;